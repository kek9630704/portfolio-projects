/** @type {import('tailwindcss').Config} */
import plugin from 'tailwindcss/plugin';

export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'pt-root-ui': ['PT Root UI', 'sans-serif'],
        'bebas-neue': ['Bebas Neue', 'sans-serif'],
        'oswald': ['Oswald', 'sans-serif']
      },
      fontWeight: {
        'pt-root-ui-bold': 'bold',
        'pt-root-ui-medium': 'normal',
        'oswald-medium': 'medium',
      },
    },
  },
  plugins: [
    plugin(function({ addBase }) {
      addBase({
        '@font-face': [
          {
            fontFamily: 'PT Root UI',
            src: `url('/src/assets/fonts/PT Root UI_Bold.otf') format('opentype')`,
            fontWeight: 'bold',
            fontStyle: 'normal',
          },
          {
            fontFamily: 'PT Root UI',
            src: `url('/src/assets/fonts/PT Root UI_Medium.otf') format('opentype')`,
            fontWeight: 'normal',
            fontStyle: 'normal',
          },
          {
            fontFamily: 'Bebas Neue',
            src: `url('/src/assets/fonts/BebasNeue-Regular.ttf') format('truetype')`,
            fontWeight: 'normal',
            fontStyle: 'normal',
          },
          {
            fontFamily: 'Oswald',
            src: `url('/src/assets/fonts/Oswald-VariableFont_wght.ttf') format('truetype')`,
            fontWeight: 'normal',
            fontStyle: 'normal',
          },
          {
            fontFamily: 'Oswald',
            src: `url('/src/assets/fonts/static/Oswald-Medium.ttf') format('truetype')`,
            fontWeight: 'normal',
            fontStyle: 'normal',
          },
        ],
      });
    }),
  ],
};
