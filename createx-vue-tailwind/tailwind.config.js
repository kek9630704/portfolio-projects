/** @type {import('tailwindcss').Config} */
export default {
  content: [
      "./index.html",
    "./src/**/*.{js,ts,jsx,tsx,vue}",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    fontFamily: {
      'lato': ['Lato', 'Sans-serif'],
      'ubuntu': ['Ubuntu', 'Sans-serif']
    },
    extend: {
      backgroundImage: {
        'our-services': "url('src/assets/img/bg-our-services.png')",
        'hover-services': "url('src/assets/img/hover-services.png')",

      },
    },
  },

  plugins: [
    require('flowbite/plugin'),
    require('tailwindcss-aria-attributes'),
  ],
}

