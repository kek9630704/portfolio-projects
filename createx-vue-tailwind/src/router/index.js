import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/pages/Home.vue'
import AboutUs from "@/pages/AboutUs.vue";
import Services from "@/pages/Services.vue";
import AvailablePositions from "@/pages/AvailablePositions.vue";
import News from "@/pages/News.vue";
import Contacts from "@/pages/Contacts.vue";

const routes = [
    {path: '/', name: 'Home', component: Home},
    {path: '/aboutus', name: 'AboutUs', component: AboutUs},
    {path: '/services', name: 'Services', component: Services},
    {path: '/available-positions', name: 'AvailablePositions', component: AvailablePositions},
    {path: '/news', name: 'News', component: News},
    {path: '/contacts', name: 'Contacts', component: Contacts},
    {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: () => import('@/pages/NotFound.vue')
    }
]

const router = createRouter({

    history: createWebHistory(),

    routes

})

export default router